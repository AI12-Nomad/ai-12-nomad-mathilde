import threading

from common import Profile

hard_coded_profile = Profile("Nick Name")


def threaded(fn):
    def wrapper(*args, **kwargs):
        threading.Thread(target=fn, args=args, kwargs=kwargs).start()

    return wrapper
