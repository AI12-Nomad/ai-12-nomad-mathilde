import pygame
import pygame_gui

from pygame_gui.core.ui_element import ObjectID
from client.ihm.common.component import Component
from config import config


class GamesInfo(Component):
    def __init__(
        self, pygame_manager: pygame_gui.UIManager, container: Component
    ) -> None:
        super().__init__(pygame_manager)
        self.pygame_manager = pygame_manager

        self.set_width(600)
        self.set_height(100)

        self.set_pos_x(700 // 2 - 300)
        self.set_pos_y(260)

        self.container = container
        self.games_won = 0
        self.games_lost = 0
        self.games_draw = 0

    def render(self) -> None:
        self.gui_element = pygame_gui.elements.UIPanel(
            relative_rect=pygame.Rect(
                (self.pos_x, self.pos_y), (self.width, self.height)
            ),
            manager=self.pygame_manager,
            container=self.container.gui_element,
            starting_layer_height=1,
            object_id=ObjectID(class_id="@options_panel"),
        )

        total_label = pygame_gui.elements.UITextBox(
            relative_rect=pygame.Rect((0, 10), (150, 0)),
            html_text="Parties jouées",
            wrap_to_height=True,
            manager=self.manager,
            container=self.gui_element,
            object_id=ObjectID(class_id="@profile_games_label"),
        )

        total = pygame_gui.elements.UITextBox(
            relative_rect=pygame.Rect((50, 50), (150, 0)),
            html_text=str(self.games_won + self.games_draw + self.games_lost),
            wrap_to_height=True,
            manager=self.manager,
            container=self.gui_element,
            object_id=ObjectID(class_id="@profile_games_number"),
        )

        won_label = pygame_gui.elements.UITextBox(
            relative_rect=pygame.Rect((150, 10), (150, 0)),
            html_text="Parties gagnées",
            wrap_to_height=True,
            manager=self.manager,
            container=self.gui_element,
            object_id=ObjectID(class_id="@profile_games_label"),
        )

        won = pygame_gui.elements.UITextBox(
            relative_rect=pygame.Rect((200, 50), (100, 0)),
            html_text=str(self.games_won),
            wrap_to_height=True,
            manager=self.manager,
            container=self.gui_element,
            object_id=ObjectID(class_id="@profile_games_number"),
        )

        draw_label = pygame_gui.elements.UITextBox(
            relative_rect=pygame.Rect((300, 10), (150, 0)),
            html_text="Parties égalisées",
            wrap_to_height=True,
            manager=self.manager,
            container=self.gui_element,
            object_id=ObjectID(class_id="@profile_games_label"),
        )

        draw = pygame_gui.elements.UITextBox(
            relative_rect=pygame.Rect((350, 50), (100, 0)),
            html_text=str(self.games_draw),
            wrap_to_height=True,
            manager=self.manager,
            container=self.gui_element,
            object_id=ObjectID(class_id="@profile_games_number"),
        )

        lost_label = pygame_gui.elements.UITextBox(
            relative_rect=pygame.Rect((450, 10), (150, 0)),
            html_text="Parties perdues",
            wrap_to_height=True,
            manager=self.manager,
            container=self.gui_element,
            object_id=ObjectID(class_id="@profile_games_label"),
        )

        lost = pygame_gui.elements.UITextBox(
            relative_rect=pygame.Rect((500, 50), (150, 0)),
            html_text=str(self.games_lost),
            wrap_to_height=True,
            manager=self.manager,
            container=self.gui_element,
            object_id=ObjectID(class_id="@profile_games_number"),
        )

        line_one = pygame_gui.elements.UIPanel(
            relative_rect=pygame.Rect((140, 0), (1, 100)),
            manager=self.pygame_manager,
            container=self.gui_element,
            starting_layer_height=1,
            object_id=ObjectID(class_id="@profile_line"),
        )

        line_two = pygame_gui.elements.UIPanel(
            relative_rect=pygame.Rect((290, 0), (1, 100)),
            manager=self.pygame_manager,
            container=self.gui_element,
            starting_layer_height=1,
            object_id=ObjectID(class_id="@profile_line"),
        )

        line_three = pygame_gui.elements.UIPanel(
            relative_rect=pygame.Rect((450, 0), (1, 100)),
            manager=self.pygame_manager,
            container=self.gui_element,
            starting_layer_height=1,
            object_id=ObjectID(class_id="@profile_line"),
        )

    def set_games_info(self, games_won: int, games_lost: int, games_draw: int) -> None:
        self.games_won = games_won
        self.games_lost = games_lost
        self.games_draw = games_draw
        # self.games_won = 1
        # self.games_lost = 2
        # self.games_draw = 3
        self.render()
