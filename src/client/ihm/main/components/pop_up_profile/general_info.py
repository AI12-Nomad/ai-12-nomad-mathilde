import pygame
import pygame_gui

from pygame_gui.core.ui_element import ObjectID
from client.ihm.common.component import Component
from config import config


class GeneralInfo(Component):
    def __init__(
        self, pygame_manager: pygame_gui.UIManager, container: Component
    ) -> None:
        super().__init__(pygame_manager)
        self.pygame_manager = pygame_manager

        self.set_width(300)
        self.set_height(250)

        self.set_pos_x(700 // 2 - 150)
        self.set_pos_y(5)

        self.text = ""
        self.container = container

    def render(self) -> None:
        self.gui_element = pygame_gui.elements.UIPanel(
            relative_rect=pygame.Rect(
                (self.pos_x, self.pos_y), (self.width, self.height)
            ),
            manager=self.pygame_manager,
            container=self.container.gui_element,
            starting_layer_height=1,
            object_id=ObjectID(class_id="@options_panel"),
        )

        image = pygame_gui.elements.UIImage(
            relative_rect=pygame.Rect((300 // 2 - 60, 25), (100, 100)),
            image_surface=pygame.image.load("./ressources/images/hen.png"),
            manager=self.pygame_manager,
            container=self.gui_element,
        )

        text = pygame_gui.elements.UITextBox(
            relative_rect=pygame.Rect((300 // 2 - 75, 150), (150, 0)),
            html_text=self.text,
            wrap_to_height=True,
            manager=self.manager,
            container=self.gui_element,
            object_id=ObjectID(class_id="@profile_name"),
        )

    def set_name(self, nickname: str) -> None:
        self.text = nickname
        self.render()
