import pygame_gui
import typing

from client.ihm.common.pop_up import PopUp
from client.ihm.main.components.pop_up_profile.close_button import CloseButton
from client.ihm.main.components.pop_up_profile.games_info import GamesInfo
from client.ihm.main.components.pop_up_profile.general_info import GeneralInfo
from client.ihm.main.components.pop_up_profile.profile_window import ProfileWindow

from common import Profile

from pygame_gui.elements.ui_window import UIWindow


class ProfileView(PopUp):
    def __init__(
        self, pygame_manager: pygame_gui.UIManager, ui, controller: typing.Any
    ):
        super(PopUp, self).__init__(pygame_manager, ui, controller)

        self.profile_window = ProfileWindow(pygame_manager)
        self.close_button = CloseButton(pygame_manager, self.profile_window)
        self.general_info = GeneralInfo(pygame_manager, self.profile_window)
        self.games_info = GamesInfo(pygame_manager, self.profile_window)

        self.add(self.profile_window)
        self.add(self.close_button)
        self.add(self.general_info)
        self.add(self.games_info)

    def set_profile(self, profile: Profile) -> None:
        self.profile = profile
        self.general_info.set_name(self.profile.nickname)
        self.games_info.set_games_info(
            self.profile.games_won, self.profile.games_lost, self.profile.games_draw
        )
