from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from client.ihm.game.ihm_game_controller import IHMGameController
from common.interfaces.i_comm_calls_ihm_game import I_CommCallsIHMGame


class CommCallsIHMGame_Impl(I_CommCallsIHMGame):

    """

    Interface Comm Calls IHM Game

    """

    def __init__(self, controller: IHMGameController):
        self.controller = controller

    def notify_new_spectator(self) -> None:
        pass

    def spectator_quits_game(self) -> None:
        pass

    def update_game(self) -> None:
        print("handle_move\n")
        local_game = self.controller.get_my_interface_to_data().get_local_game()
        self.controller.local_game = local_game
        self.controller.update_game_board(local_game.board)
        self.controller.update_tile_number()
        self.controller.define_current_player()
        self.controller.show_game_view()
        if self.controller.get_my_interface_to_data().is_local_game_finished():
            print("game finished detected\n")
            if (
                len(self.controller.get_my_interface_to_data().get_local_game_winner())
                == 2
            ):
                self.controller.pygame_controller.update_component(
                    self.controller.game_view.draw_pop_up.draw_pop_up_component
                )
                self.controller.show_draw_window()
                print("draw")
            else:
                winner = (
                    self.controller.get_my_interface_to_data()
                    .get_local_game_winner()[0]
                    .convertToPlayer()
                )
                winner_component = (
                    self.controller.game_view.winner_pop_up.winner_pop_up_component
                )
                winner_component.set_winner(winner)
                self.controller.pygame_controller.update_component(winner_component)
                self.controller.show_winner_window()
                print("winner")
        else:
            self.controller.update_game_board(local_game.board)
            self.controller.update_tile_number()
            self.controller.define_current_player()
            self.controller.show_game_view()

    def notify_new_message(self) -> None:
        pass
